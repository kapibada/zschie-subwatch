#! /usr/bin/env python3

# Copyright 2020 Radosław Wyrzykowski
# This work is licensed under the terms of the EUPL, version 1.2.
# You may obtain a copy of this license in any of the languages of the
# European Union at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

from datetime import date, timedelta, datetime
from time import sleep
from os import environ
import os
from filecmp import cmp
import wget
import urllib
from Mailer import assemblesend

# Podaj klasę, dla której zamierzasz pobierać zastępstwa
# W tym momencie wspierane są klasy mające grupę 1 i 2.
klasa = ''
# Podaj adres email, na który będą wysyłane informacje
dokąd = ''
# Robimy słownik przypisujący nazwiska nauczycieli do przedmiotów w miejscowniku
# Wypełnij klucze nazwiskami nauczycieli w formacie "A. Kowalski"
# Księża w formacie "ks. A. Nowak"
na_czym = {
    'A. Kowalski': 'matematyce',
    'ks. B. Nowak': 'religii'
}


def pull(jutro):
    """
    Ściąga plik z zastępstwami ze strony ZSChiE i przygotowuje do przetwarzania.
    Parametr jutro decyduje, czy ściągany jest plik na dzisiaj, czy na jutro.
    """
    if jutro:
        data = date.today() + timedelta(days=1)
        dkrotka = data.timetuple()
        nazwa_pliku = f"{dkrotka.tm_mday:02}.{dkrotka.tm_mon:02}.{dkrotka.tm_year}.pdf"
    else:
        data = date.today()
        dkrotka = data.timetuple()
        nazwa_pliku = f"{dkrotka.tm_mday:02}.{dkrotka.tm_mon:02}.{dkrotka.tm_year}.pdf"
    if data.isoweekday() > 5:
        print('Weekend!')
        return(0)
    url = f"http://zastepstwa.zschie.pl/pliki/{nazwa_pliku}"
    # Sprawdzamy, czy już sprawdzaliśmy.
    if os.path.isfile(f"/usr/src/app/Zastępstwa/{nazwa_pliku}"):
        ścieżka = f"/usr/src/app/Zastępstwa/copy-{nazwa_pliku}"
        staraścieżka = f"/usr/src/app/Zastępstwa/{nazwa_pliku}"
    else:
        ścieżka = f"/usr/src/app/Zastępstwa/{nazwa_pliku}"
        staraścieżka = None
    # Ściągamy… Jeżeli nie ma pliku, nie wysypujemy się, tylko czekamy i próbujemy ponownie później.
    try:
        wget.download(url, ścieżka)
    except urllib.error.HTTPError:
        print('Nie udało się pobrać pliku')
        return(1)
    if staraścieżka:
        if cmp(ścieżka, staraścieżka):
            os.remove(ścieżka)
            print("Nic się nie zmieniło")
            return(0)
        else:
            assemblesend(ścieżka, dkrotka, klasa, na_czym, dokąd,
                         True, newfile=f"copy-{nazwa_pliku}")
            os.replace(ścieżka, staraścieżka)
    else:
        assemblesend(ścieżka, dkrotka, klasa, na_czym, dokąd, False)


while True:
    # Pamiętaj, że w kontenerze strefa czasowa to UTC!
    if datetime.now().hour >= 0 and datetime.now().hour < 17:
        pull(jutro=False)
        print('Sprawdzę ponownie za godzinę…')
        sleep(3600)
    elif datetime.now().hour >= 17 and datetime.now().hour < 24:
        pull(jutro=True)
        print('Sprawdzę ponownie za godzinę…')
        sleep(3600)
