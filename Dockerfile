FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN apt-get update && apt-get install -y --no-install-recommends default-jre-headless
RUN pip install --no-cache-dir -r requirements.txt
RUN mkdir /usr/src/app/Zastępstwa
COPY . /usr/src/app
ENTRYPOINT ["python", "-u", "Subwatch.py"]