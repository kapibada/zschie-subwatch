Contributions are welcome, but please note that contributing requires you to license your contributions under the project's license.

Przyjmuję wkład innych, natomiast cały kod, jaki chcesz włączyć do projektu musi być objęty jego licencją.