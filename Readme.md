# Subwatch

Licencja tego projektu to EUPL w wersji 1.2


To jest skrypt, który sprawdza zastępstwa klasy 3 ai (w roku 2019/2020) ZSChiE w Gdyni i rozysła je zainteresowanym…  
To znaczy sprawdzał i rozsyłał, dopóki na przełomie października i listopada 2019 dyrekcja bez ostrzeżenia ani wyjaśnienia
przestała publikować te dane na stronie szkoły.  
Jako twórcy tego kodu, przez ostatnie miesiące narastało we mnie zmęczenie - po co chodzić na drugie piętro do zastępstw,
skoro do tej pory przychodziły one do ciebie na skrzynkę? I dlaczego w szkole informatycznej utrudnia się stosowanie informatyki w życiu codziennym?  
Aby wyrazić niezadowolenie moje i moich kolegów, zdecydowałem się poświęcić temu trochę czasu i w końcu opublikować to.
Wiem, że być może gdybym to zrobił wcześniej, decyzja dyrekcji mogłaby się spotkać z ostrzejszym odzewem.
Cóż, zawsze jest coś innego do zrobienia.

W każdym razie, ten skrypt potrafi ściągnąć kartkę z zastępstwami, wyciągnąć z niej wiersze, które dotyczą danej klasy i wysłać na wybrany adres email.  
Przykładowa wiadomość:  
DRUGA GRUPA: Na systemach operacyjnych (7.) utk z nauczycielem K. Kociuba w sali 212.  
DRUGA GRUPA: Na systemach operacyjnych (8.) sieci komp. z nauczycielem A. Śledzińska w sali 212.  
DRUGA GRUPA: Na systemach operacyjnych (9.) bazy danych z nauczycielem K. Kasperkowiak w sali 212.  
(I w załączniku sama kartka)

Jeżeli w trakcie ukaże się nowa kartka, na której zaszły zmiany, które dotyczą twojej klasy, dostaniesz nowego maila.  
Kartki są sprawdzane co godzinę.

# Jak używać

1. Doprowadź do przywrócenia dostępu do zastępstw w Internecie. (Nawołuj do głośnego narzekania!!!)
2. Przygotuj:
    - Komputer, który może chodzić 24/7. Na komputerze (czy też serwerze) zainstaluj Dockera. (Ten obraz nie był testowany na platformie ARM (Raspberry Pi i podobne)!)
    - Konto mailowe, z którego będziesz wysyłał maile. (W wypadku Gmaila, pamiętaj o włączeniu mniej bezpiecznych aplikacji w ustawieniach konta Google, na karcie zabezpieczenia. Jeżeli używasz dwuetapowej weryfikacji, utwórz hasło aplikacji i zapisz je gdzieś)
    - Listę mailingową, do której zaprosisz kolegów i koleżanki z klasy (Ja używam Grup Google)
3. Ściągnij kod źródłowy tego repozytorium. Jeśli możesz, użyj do tego Gita - ułatwi to aktualizacje, w razie zmian w kodzie.
4. Wprowadź zmiany w pliku Subwatch.py według instrukcji w nim zawartych.
5. Uruchom w folderze z kodem komendę: docker build -t {tutaj wpisz swój nick}/subwatch . (Jeżeli nie działa, uruchom jako administrator/root, lub dodaj siebie do grupy docker)
6. Uruchom komendę: docker run --name subwatch --env email_from={tutaj adres, z którego wysyłasz, w formacie: Bot od zastępstw\<zastepstwazschie@example.com\>} --env email_smtp={tutaj adres twojego serwera SMTP} --env email_smtp_user={tutaj nazwa użytkownika na emailu} --env email_smtp_pass={Tutaj hasło do emaila} --restart=always {tutaj wpisz swój nick}/subwatch
7. Używaj komendy: docker logs --timestamps subwatch, aby zobaczyć, czy nie ma błędów. Powtórz to dzień później, na wszelki wypadek, a potem najlepiej co jakiś czas.

(Jeżeli potrzebujesz interfejsu graficznego dla Dockera, polecam [Portainer](https://www.portainer.io/).)