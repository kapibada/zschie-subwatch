#! /usr/bin/env python3

# Copyright 2020 Radosław Wyrzykowski
# This work is licensed under the terms of the EUPL, version 1.2.
# You may obtain a copy of this license in any of the languages of the
# European Union at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

from io import StringIO
from pathlib import Path
from os import environ
import os
import tabula
import pandas as pd
import re
import smtplib
import ssl
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_mail(email_from, email_to, subject, text, files=None,
              server="127.0.0.1", login=None, password=None):
    message = MIMEMultipart()
    message["From"] = email_from
    message["To"] = email_to
    message["Subject"] = subject
    message.attach(MIMEText(text, 'plain'))
    for file in files:
        with open(file, 'rb') as attachment:
            part = MIMEApplication(attachment.read(), Name=basename(file))
        part.add_header('Content-Disposition',
                        f'attachment; filename={basename(file)}')
        message.attach(part)

    messagetext = message.as_string()
    sslcontext = ssl.create_default_context()
    with smtplib.SMTP_SSL(server, 465, context=sslcontext) as conn:
        conn.login(login, password)
        conn.sendmail(email_from, email_to, messagetext)


def assemblesend(ścieżka, data, klasa, na_czym, to, aktualizacja=False, **kwargs):
    """
    Assemble substitutions table and send an email about it.
    Ścieżka takes a path or path-like object pointing to a PDF from zastepstwa.zschie.pl
    Data takes a timetuple corresponding to the date on the file
    Aktualizacja takes a boolean value determining whether the email is an update of a previous one
    If aktualizacja is set to True, provide the file name of the up-to-date PDF in newfile
    This asinine thing is a weird hack, the reasons for which I really don't understand anymore...
    """
    zwol_re = re.compile(
        r'grupa na \d godz. lekcyjną|klasa na \d godz. lekcyjną')
    nazwa_pliku_akt = f"{data.tm_mday:02}.{data.tm_mon:02}.{data.tm_year}.pdf.txt"
    nazwa_pliku_pdf = f"{data.tm_mday:02}.{data.tm_mon:02}.{data.tm_year}.pdf"
    if aktualizacja is True:
        nazwa_pliku_pdf = kwargs.get('newfile')
        if nazwa_pliku_pdf is None:
            print("We're updating, but we didn't get a file…?")
            print("Let's try to prevent a crash and not update. If you see this message, please investigate and report a bug.")
            aktualizacja = False
    try:
        tabela = tabula.read_pdf(ścieżka, pages='all')
    except FileNotFoundError:
        print("…But there was no file!")
        return 2
    wiadomość = StringIO()
    # Odczytujemy to, co zapisaliśmy wcześniej...
    if aktualizacja is True:
        with open(f'/usr/src/app/Zastępstwa/{nazwa_pliku_akt}', 'r') as stare:
            istniejące = stare.read()
    # Ustawiamy nazw kolumn i usuwamy niepotrzebne rekordy
    kolumny = {
        'Unnamed: 0': 'Lp.',
        'Unnamed: 1': 'Nieobecny',
        'Unnamed: 2': 'Klasa',
        'Numer': 'Lekcja',
        'Unnamed: 4': 'Zastępca',
        'Unnamed: 5': 'Przedmiot',
        'Unnamed: 6': 'Sala'
    }
    tabela.rename(columns=kolumny, inplace=True)
    tabela = tabela.drop(tabela.index[0:4])
    tabela = tabela.reset_index(level=0, drop=True)
    # Ustawiamy nazwiska nauczycieli we wszystkich wierszach tabeli
    tabela.Nieobecny.fillna(method='ffill', inplace=True)
    # Składamy wiadomość
    licz = 0
    for row in tabela.itertuples(name=None):
        nauczyciel = row[2]
        co_jest = na_czym.get(nauczyciel, 'czymś')
        if row[3] == klasa:
            if row[5] == 'klasa zwolniona':
                wpis = f'Na {co_jest} ({row[4]}.) KLASA ZWOLNIONA. \n'
            else:
                if zwol_re.match(row[5]):
                    wpis = f'{row[5]}\n'
                else:
                    wpis = f'Na {co_jest} ({row[4]}.) {row[6]} z nauczycielem {row[5]} w sali {row[7]}.\n'
            licz = licz+1
        elif row[3] == f'{klasa} gr I':
            if row[5] == 'grupa zwolniona':
                wpis = f'Na {co_jest} ({row[4]}.) PIERWSZA GRUPA ZWOLNIONA.\n'
            else:
                if zwol_re.match(row[5]):
                    wpis = f'PIERWSZA GRUPA: {row[5]}\n'
                else:
                    wpis = f'PIERWSZA GRUPA: Na {co_jest} ({row[4]}.) {row[6]} z nauczycielem {row[5]} w sali {row[7]}.\n'
            licz = licz+1
        elif row[3] == f'{klasa} gr II':
            if row[5] == 'grupa zwolniona':
                wpis = f'Na {co_jest} ({row[4]}.) DRUGA GRUPA ZWOLNIONA.\n'
            else:
                if zwol_re.match(row[5]):
                    wpis = f'DRUGA GRUPA: {row[5]}\n'
                else:
                    wpis = f'DRUGA GRUPA: Na {co_jest} ({row[4]}.) {row[6]} z nauczycielem {row[5]} w sali {row[7]}.\n'
            licz = licz+1
        else:
            wpis = None
        if wpis:
            # Czy to w ogóle działa?
            if aktualizacja is True:
                if wpis not in istniejące:
                    print(f'NOWE: {wpis}')
                    wiadomość.write(wpis)
                else:
                    print(wpis)
                    licz -= 1
            else:
                print(wpis)
                wiadomość.write(wpis)
#        if wpis:
#            if wpis not in istniejące:
#                wiadomość.write('NOWE: ')
#            wiadomość.write(wpis)
    if licz == 0:
        print('Jutro nie ma zastępstw.')
    wiadomość.seek(0)
    # Zapisujemy treść bez stopki na później.
    with open(f'/usr/src/app/Zastępstwa/{nazwa_pliku_akt}', 'w') as nowe:
        nowe.write(wiadomość.read())
    if licz == 0:
        return (0)
    # Wysyłamy maila
    wiadomość.write(
        'Ta wiadomość została wygenerowana W PEŁNI automatycznie. Lepiej na nią nie odpowiadać.')
    if aktualizacja:
        subject = '**Aktualizacja** Zastępstwa (BETA)'
    else:
        subject = 'Jutrzejsze zastępstwa (BETA)'
    wiadomość.seek(0)
    contents = wiadomość.read()
    print(contents)
    send_mail(environ['email_from'], to, subject, contents, [
              f'/usr/src/app/Zastępstwa/{nazwa_pliku_pdf}'], environ['email_smtp'], environ['email_smtp_user'], environ['email_smtp_pass'])
    print('Wysłano…')
    return(0)
